# README #

This README would normally document whatever steps are necessary to get your application up and running.

This Repo is for the data used in a commentary to Brown and Thomson 2016, regarding using likelihood for finding genes with disproportionate influence.

### Commands used ###

#Obtaining data

wget http://datadryad.org/bitstream/handle/10255/dryad.107733/alignments.zip?sequence=1

mv alignments.zip\?sequence=1 alignments.zip

unzip alignments.zip

Made trees for each topology from Chiari = BrownTrees.tre

pxcat -s *.nex -p Genes.model -o Concatenated_Chiari.fa

#Run SSLL

raxmlHPC-PTHREADS-AVX2 -f g -T 2 -s Concatenated_Chiari.fa-q Genes.model -m GTRGAMMA -z BrownTrees.tre -n SSLL

Invert the data to make it easier for R

Remove Header

cp RAxML_perSiteLLs.SSLL NoHeader.txt

perl Switch_Row_Col.pl NoHeader.txt > Inverted.txt

Get the gene start and stop combined

perl RcodeForGenes.pl Genes.model > GeneStartStop.txt

Open R code SSLL_code.R

#Make individual gene trees

for x in *.nex;do pxs2fa -s $x -o $x.fa;done

for x in *.fa;do perl ChangeFastaName.pl $x;done

for x in *.fa;do raxml -T 36 -p 12345 -m GTRCAT -s $x -n $x.tre;done

Format for Sh Test

Basic Regex in perl to change the names

for x in *.tre;do perl Rename.pl $x;done

231 of the 248 trees ran some trees only had missing data for taxa

Move all complete ones to a separate folder

for x in *.tre;do perl Rename.pl $x;done

Remove the taxa that are made up of only missing data

for x in *.fa;do raxml -T 36 -p 12345 -m GTRCAT -s $x -n $x.tre;done

Run Shtest

for x in *.fa;do raxml -T 9 -f J -p 12345 -m GTRGAMMA -s $x -t $x.tre -n $x.SH;done

Rename the RaxML output

Adjust the Sh

for x in *;do python ../../../../CarnivoryReanalyze/Programs/move_notes_labels_trees_raxml.py $x > $x.sh;done

reroot

for x in *.sh;do pxrr -t $x -g protopterus -o $x.rr;done

java -jar phyparts-0.0.1-SNAPSHOT-jar-with-dependencies.jar -m GeneTrees/HasOutgroup/ -d SpeciesTreeTurt.tre -s 80 -v -a 1

head out.concon.tre -n 1 > Concordant.tre

head out.concon.tre -n 2 | tail -n 1 > Conflict.tre


Make pie charts: python phypartspiecharts.py SpeciesTreeTurt.tre Vertebrate/out 93 --svg_name test.svg


#Run SSLL on a single gene to remake Jeremys result

Removed: protopterus,podarcis,alligator,python

Renamed 8916_Tre.tre

raxml -f g -T 2 -s ENSGALG00000008916.fa  -m GTRGAMMA -z 8916_Tre.tre -n 8916

Tree 0: -3365.466682

Tree 1: -3447.758668

#Run SSLL on a single gene to remake Jeremys result

Removed: protopterus,podarcis,python,caiman,chelonoidis_nigra,emys_orbicularis

pxrmt -t BrownTrees.tre -n protopterus,podarcis,python,caiman,chelonoidis_nigra,emys_orbicularis -o 1362_Tre.tre

raxml -f g -T 2 -s ENSGALG00000001362.fa  -m GTRGAMMA -z 1362_Tre.tre -n 1362

Tree 0: -2788.028501

Tree 1: -2807.699611

Make Species Trees

raxml -T 9 -f a -# 200 -m GTRCAT -p 12345 -x 112233 -s Concatenated_Chiari.fa -q Genes.model -n All

Remove 8916, 11434

raxml -T 9 -f a -# 200 -m GTRCAT -p 12345 -x 112233 -s Miss2.fa -q Miss2Genes.model -n Miss2

Results of Species Tree show those two genes alter the topology


#Find species bipartition

Bip 0: grep "Xenopus" * | grep "Homo" | grep "Monodelphis" | grep "Ornithorhynchus" | grep "Gallus" | grep "Taeniopygia" | grep "alligator" | grep "caiman" | grep "phrynops" |grep "caretta" | grep "emys_orbicularis" | grep "chelonoidis_nigra" | grep "podarcis" | grep "python" | grep "Anolis" | wc -l

Bip 1: grep "Homo" * | grep "Monodelphis" | grep "Ornithorhynchus" | grep "Gallus" | grep "Taeniopygia" | grep "alligator" | grep "caiman" | grep "phrynops" |grep "caretta" | grep "emys_orbicularis" | grep "chelonoidis_nigra" | grep "podarcis" | grep "python" | grep "Anolis" | wc -l

Bip 2: grep "Homo" * | grep "Monodelphis" | grep "Ornithorhynchus" | wc -l

Bip 3: grep "Homo" * | grep "Monodelphis" | wc -l

Bip 4: grep "Gallus" * | grep "Taeniopygia" | grep "alligator" | grep "caiman" | grep "phrynops" |grep "caretta" | grep "emys_orbicularis" | grep "chelonoidis_nigra" | grep "podarcis" | grep "python" | grep "Anolis" | wc -l

Bip 5: grep "Gallus" * | grep "Taeniopygia" | grep "alligator" | grep "caiman" | grep "phrynops" |grep "caretta" | grep "emys_orbicularis" | grep "chelonoidis_nigra"| wc -l

Bip 6: grep "Gallus" * | grep "Taeniopygia" | wc -l

Bip 7: grep "alligator" * | grep "caiman" | grep "phrynops" |grep "caretta" | grep "emys_orbicularis" | grep "chelonoidis_nigra" | wc -l

Bip 8: grep "alligator" * | grep "caiman" | wc -l

Bip 9: grep "phrynops"  *|grep "caretta" | grep "emys_orbicularis" | grep "chelonoidis_nigra"| wc -l

Bip 10: grep "caretta" * | grep "emys_orbicularis" | grep "chelonoidis_nigra"| wc -l

Bip 11:  grep "emys_orbicularis" * | grep "chelonoidis_nigra"| wc -l

Bip 12: grep "podarcis" * | grep "python" | grep "Anolis" | wc -l

Bip 13: grep "python" * | grep "Anolis" | wc -l

Alt Bip: grep "Gallus" * | grep "Taeniopygia" | grep "phrynops" |grep "caretta" | grep "emys_orbicularis" | grep "chelonoidis_nigra" | wc -l

Testing all topologies

cat Species_tree Genetree > all.tre
pxbp -t all.tre -e -v > bp.log
raxmlHPC-PTHREADS-AVX2 -s ALIGNMENT -f G -m GTRGAMMA -T 2 -n SITECALC -z GENETREES 
python process_likes_splits_sites.py GENETREES RAxML_perSiteLLs.SITECALC bp.log